clear all
close all
clc
%%




%%%%%%%%%%%%%%%%
%%% Setings %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot_on = 1;
agent_numbers = 20;         % agent_numbers
type = 'IDM';               % Driving model (OVM/IDM)
v_0 = 0.1;                   % maximum initial velocity (m/s)
r = 69.44;                   % ring radius (m)
L = 5;
eps = 0.001;
dt = 0.1;
motion_pace = 1;            % fast forward (1 = normal)
simulation_duration = 500;        % (s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%



A = zeros(2*agent_numbers);
x = zeros(2*agent_numbers,1);
a = zeros(agent_numbers,1);
vel_vector = zeros(agent_numbers,1);
pos_vector = zeros(agent_numbers,1);
hdw_vector = zeros(agent_numbers,1);
pos_st = zeros(simulation_duration/dt,agent_numbers);
vel_st = zeros(simulation_duration/dt,agent_numbers);
hdw_st = zeros(simulation_duration/dt,agent_numbers);

% build a random matrix from random values
rand_vector = rand(agent_numbers,1)*(2*pi*r);
% rand_vector(1) = 0.1; %*(2*pi*r);
% rand_vector(2) = 0.2; %*(2*pi*r);
% rand_vector(3) = 0.3; %*(2*pi*r);
% sort the values of random vector
rand_vector = sort(rand_vector,'descend');

% fill state matrix with initial random values
for i=1:agent_numbers
    x(2*i-1) = rand_vector(i);
    x(2*i) = rand()*v_0;
    
end
%% 


% fig_ring = figure('Position', [1200, 100, 500, 500]);
% fig_ring.Name = 'Visualization';
% %fig_ring.Visible = 'off';
%     %ax1 = polaraxes;
%     polarplot(0, r, 'o', 'LineWidth',2);
%     hold on 


% start simiulation!
for t=0:1:simulation_duration/dt

    for i=1:agent_numbers 
        j = i-1; % agent j is the lead agent
        if j == 0 % close the agents' chain
            j = agent_numbers;
            % obtain acceleration of the first agent from DRIVER model
            % (first agent is an exeption because it should be connected to the last agent)
            a(i) = DRIVER(type, x(2*i-1), x(2*j-1)+2*pi*r, x(2*i), x(2*j));
        else
            % obtain acceleration of the agents from DRIVER model
            a(i) = DRIVER(type, x(2*i-1), x(2*j-1), x(2*i), x(2*j));
        end
        
        % update positions and velocities
        x(2*i-1) = x(2*i-1) + x(2*i)*dt + (1/2)*a(i)*dt^2 ;
        x(2*i) = x(2*i) + a(i)*dt;
        
%         if t<1.5 && i==2
%             x(2*i-1) = 7;
%             x(2*i) = 0;
%         end
%         if t<4 && i==3
%             x(2*i-1) = 1;
%             x(2*i) = 0;
%         end

        vel_vector(i) = x(2*i);
        pos_vector(i) = x(2*i-1);
        if i == 1
            hdw_vector(i) = (x(2*agent_numbers-1)+2*pi*r) - x(1) - L;
        else
            hdw_vector(i) = x(2*(i-1)-1) - x(2*i-1) - L;
        end
        
        
    end    
    pos_st(t+1,:) = pos_vector;
    vel_st(t+1,:) = vel_vector;
    hdw_st(t+1,:) = hdw_vector;
 
%     ax1 = polaraxes('Parent', fig_ring);
%     r_vector = r*ones(agent_numbers,1);
%     polarscatter(ax1, pos_vector/r, r_vector, 'o', 'filled');

    
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% linearization about the operating point 't' %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for i=1:agent_numbers

        j = i-1; % agent j is the leader agent
        if j == 0; j = agent_numbers; end % close the agents' chain

        % partial numerical derivation from states of the leader(j) and follower(i) agents
        alpha_x_i_respect2_i = (DRIVER(type, x(2*i-1)+eps, x(2*j-1), x(2*i), x(2*j)) - DRIVER(type, x(2*i-1), x(2*j-1), x(2*i), x(2*j)))/eps;
        alpha_v_i_respect2_i = (DRIVER(type, x(2*i-1), x(2*j-1), x(2*i)+eps, x(2*j)) - DRIVER(type, x(2*i-1), x(2*j-1), x(2*i), x(2*j)))/eps;
        alpha_x_j_respect2_i = (DRIVER(type, x(2*i-1), x(2*j-1)+eps, x(2*i), x(2*j)) - DRIVER(type, x(2*i-1), x(2*j-1), x(2*i), x(2*j)))/eps;
        alpha_v_j_respect2_i = (DRIVER(type, x(2*i-1), x(2*j-1), x(2*i), x(2*j)+eps) - DRIVER(type, x(2*i-1), x(2*j-1), x(2*i), x(2*j)))/eps;

        % build the A matrix
        A(2*i-1, 2*i) = 1;
        A(2*i, 2*i-1) = alpha_x_i_respect2_i;
        A(2*i, 2*i) = alpha_v_i_respect2_i;
        A(2*i, 2*j-1) = alpha_x_j_respect2_i;
        A(2*i, 2*j) = alpha_v_j_respect2_i;
        
    end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 

        %pause (dt/motion_pace);
       % ax1 = polaraxes('Parent', fig_ring);
        % hold (ax1, 'off')
        
    
end


time = linspace(0,simulation_duration,simulation_duration/dt+1);

fig_pos = figure('Position', [50, 50, 800, 400]);
fig_pos.Name = 'Position';
    for i=1:agent_numbers
    plot(time,pos_st(:,i),'b-');
    hold on
    end
    xlim([0 simulation_duration])
    %ylim([0 simulation_duration*r])
    xlabel('time(s)')
    ylabel('positions(m)')

fig_vel = figure('Position', [50, 580, 800, 400]);
fig_vel.Name = 'Velocities';
    for i=1:agent_numbers
    plot (time,vel_st(:,i),'r-');
    hold on
    end
    xlim([0 simulation_duration])
    %ylim([0 20])
    xlabel('time(s)')
    ylabel('velocity(m/s)')
    
fig_hdw = figure('Position', [900, 580, 800, 400]);
fig_hdw.Name = 'Headways (Spacing)';
    for i=1:agent_numbers
    plot (time,hdw_st(:,i),'k-');
    hold on
    end
    xlim([0 simulation_duration])
    %ylim([0 20])
    xlabel('time(s)')
    ylabel('headway(m)')
%% 

