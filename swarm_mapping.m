clear all
clc
close all
global N r_r r_o r_a communication_range theta_max COM
N = 40;
r_r = 1.4;
r_o = 2.2;
r_a = 3.9;
communication_range = 2;
theta_max = 10;
COM = zeros(N);
area_height = 10;
area_width = 10;
pos = [area_width*rand(N,1) area_height*rand(N,1)];
head = 2*pi*rand(N,1);
vel = [cos(head) sin(head)];
dt = 0.05;
simulation_duration = 10;
scatter(pos(:,1),pos(:,2));


for t= 0:dt:simulation_duration

% calculate interactions between agents
[D] = swarm_interaction(pos,vel);

% update velocities based on interaction vectors
vel = swarm_velocity_update(vel,D);

% update positions
pos = pos + vel .* dt;

% show agents
scatter(pos(:,1),pos(:,2),'bo');
xlim([0 10])
ylim([0 10])
hold on
draw_headings(pos,vel,0.5);
pause(0.05);
hold off

end

function [new_vel] = swarm_velocity_update(vel,D)
global N
new_vel = zeros(size(vel));
for i= 1:N
    x1 = vel(i,1);
    x2 = D(i,1);
    y1 = vel(i,2);
    y2 = D(i,2);
    theta = atan2d(x1*y2-y1*x2,x1*x2+y1*y2);
    if abs(theta) < theta_max
        new_vel(i,:) = normalize(D(i,:),'norm');
    else
        new_vel(i,:) = rotate(vel(i,:), theta_max*sign(theta));
    end
end
end

function [D] = swarm_interaction(pos,vel)
global N r_r r_o r_a communication_range COM
D = zeros(size(pos));
COM = zeros(N);
for i= 1:N
    d_r = [0 0];
    d_o = [0 0];
    d_a = [0 0];
    n_r = 0;
    n_o = 0;
    n_a = 0;
    
    for j= 1:N
        if i ~= j
        r = pos(j,:) - pos(i,:); % relative position vector between two agents
        d = norm(r); % distnace between two agents
        r_norm = normalize(r,'norm'); % normalized relative position vector
        if d <= r_r
            d_r = d_r - r_norm;
           n_r = n_r + 1;
        elseif r_r < d && d <= r_o
            d_o = d_o + vel(j,:);
           n_o = n_o + 1;
        elseif r_o < d && d <= r_a
             d_a = d_a + r_norm;
            n_a = n_a + 1;
        end
        if d <= communication_range, COM(i,j) = 1; end
        end
    end
    
    if (n_r ~= 0) && ~(isequal(d_r,[0 0]))
        D(i,:) = d_r;
    elseif n_o ~= 0 && n_a == 0 && ~(isequal(d_o,[0 0]))
        D(i,:) = d_o;
    elseif n_a ~= 0 && n_o == 0 && ~(isequal(d_a,[0 0]))
        D(i,:) = d_a;
    elseif (n_a ~= 0) && (n_o ~= 0) && ~(isequal(d_o,[0 0])) && ~(isequal(d_a,[0 0]))
        D(i,:) = 0.5*(d_a + d_o);
    else
        D(i,:) = vel(i,:);
    end
    
end
end

function [p] = rotate(point,alpha)
R = [cosd(alpha) -sind(alpha);sind(alpha) cosd(alpha)];
p = R * point';
p = p';
end

function [] = draw_headings(pos,head,length)
N = 40;
head = normalize(head,2,'norm');
for i= 1:N
delta_x = [pos(i,1) pos(i,1)+head(i,1)*length];
delta_y = [pos(i,2) pos(i,2)+head(i,2)*length];
line(real(delta_x), real(delta_y));
hold on
end
end










