clear all
clc
close all

n = 15;
k_p = 500;
d = ones (n, 1) * ((2*pi)/n); % rad
dt = 0.0005;
Q = [-1 1 0; 0 -1 1; 1 0 -1];
P = [0;0;2*pi];
x = rand(n,1) .* ones (n,1) .* (2*pi);
x(1) = 0;
x(2) = 0.1;
x(3) = 0.2;
u_s = ones(n,1) .* 0;
X = zeros(n,1);
x = sort(x);
for i=1:10000
    for j=1:n-1
    X(j) = x(j+1) - x(j);
    end
    X(n) = (x(1)+2*pi) - x(n);
X_d = X - d;
% u_s = - X_d;
% u = Q'*(u_s-P)

A = eye(n) * k_p;
X_dot = A*X_d;

x = x + X_dot.*dt;
figure(1)
polar(x,ones(n,1),'o');
hold on
polar(x, X_d+d * (n/30));
Y = sum ((X_d).^2);
hold off
% figure(2)
% plot3(X(1),X(2),X(3))
pause(dt);
end