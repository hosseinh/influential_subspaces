# influential_subspaces
#	This repository contains:
#	1) Inteligent Driver Model (IDM) Simulation & Linearization Implemented in MATLAB
#	2) Optimal Velocity Model (OVM) Simulation & Linearization Implemented in MATLAB
#	- main_simulation.m
#	- DRIVER.m (contains both models)
#	- IDM.m
#	- OVM.m
#	Note: you can change the number of vehicles(agents) but you may need to change ring radius and their initial velocity as well in order to get an appropriate result.
#	Note: Change 'type' variable to switch between "IDM" and "OVM" in main_simulation file.
